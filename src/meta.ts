export interface IMetaData {
  title: string | null;
  description: string | null;
  keywords: string[];
}
