import cheerio from "cheerio";
import { either } from "fp-ts";

import download from "./download";
import * as Types from "./types";

export type Result = either.Either<string, Types.IGeleResult>;

export const gele = async (url: string): Promise<Result> => {
  // download
  const downloadData = await download(url);
  if (downloadData.isLeft()) {
    return either.left("download failed");
  }

  // parse
  const $ = cheerio.load(downloadData.value.data);

  // basic values
  // TODO: impl. all props
  const ogp: Types.IOgpData = {
    audio: null,
    description: $('meta[property="og:description"]').attr("content") || null,
    determiner: null,
    image: null,
    locale: null,
    localeAlternates: [],
    siteName: $('meta[property="og:site_name"]').attr("content") || null,
    title: $('meta[property="og:title"]').attr("content") || null,
    type: null,
    url: $('meta[property="og:url"]').attr("content") || null,
    video: null
  };

  // image
  const image = $('meta[property="og:image"]').attr("content") || null;
  if (image) {
    ogp.image = {
      alt: null,
      height: null,
      secureUrl: null,
      type: null,
      url: image,
      width: null
    };
  }

  // meta
  const keywordsContent = $('meta[name="keywords"]').attr("content");
  const keywords: string[] = !keywordsContent ? [] : keywordsContent.split(",");
  const meta: Types.IMetaData = {
    description: $('meta[name="description"]').attr("content") || null,
    keywords,
    title: $("title").text() || null
  };

  return either.right({ ogp, meta });
};

export default gele;
