import { IAudio } from "./audio";
import { IImage } from "./image";
import { IVideo } from "./video";

export { IAudio, IImage, IVideo };
