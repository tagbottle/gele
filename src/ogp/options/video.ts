export interface IVideo {
  url: string;
  secureUrl: string;
  type: string | null;
  width: number | null;
  height: number | null;
  alt: string | null;
}
