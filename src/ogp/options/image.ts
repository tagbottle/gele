export interface IImage {
  url: string;
  secureUrl: string | null;
  type: string | null;
  width: number | null;
  height: number | null;
  alt: string | null;
}
