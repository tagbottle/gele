export interface IAudio {
  url: string;
  secureUrl: string;
  type: string | null;
}
