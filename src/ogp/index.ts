import { IAudio, IImage, IVideo } from "./options";
import * as ObjectTypes from "./types";

export interface IOgpData {
  // basic
  title: string | null;
  type: ObjectTypes.OtherObject | null;
  image: IImage | null;
  url: string | null;

  // optional
  audio: IAudio | null;
  description: string | null;
  determiner: "a" | "an" | "the" | "" | "auto" | null;
  locale: string | null;
  localeAlternates: string[];
  siteName: string | number | null;
  video: IVideo | null;

  // object info
  article?: ObjectTypes.IArticle;
  book?: ObjectTypes.IBook;
  musicSong?: ObjectTypes.IMusicSong;
  musicAlbum?: ObjectTypes.IMusicAlbum;
  musicPlaylist?: ObjectTypes.IMusicPlaylist;
  musicRadioStation?: ObjectTypes.IMusicRadioStation;
  profile?: ObjectTypes.IProfile;
  videoMovie?: ObjectTypes.IVideoMovie;
  videoEpisode?: ObjectTypes.IVideoEpisode;
  videoTvShow?: ObjectTypes.IVideoTvShow;
  videoOther?: ObjectTypes.IVideoOther;
  webSite?: ObjectTypes.IWebSite;
}
