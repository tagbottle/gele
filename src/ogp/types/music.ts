import { Profiler } from "inspector";
import { IProfile } from "./profile";

export interface IMusicSong {
  duration: number | null;
  albums: IMusicAlbum[];
  albumDisc: number | null;
  albumTrack: number | null;
  musicians: IProfile[];
}

export interface IMusicAlbum {
  song: IMusicSong | null;
  songDisc: number | null;
  songTrack: number | null;
  musician: IProfile | null;
  releaseDate: Date | null;
}

export interface IMusicPlaylist {
  song: IMusicSong | null;
  songDisc: number | null;
  songTrack: number | null;
  creator: IProfile | null;
}

export interface IMusicRadioStation {
  creator: IProfile | null;
}
