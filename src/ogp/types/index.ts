export * from "./article";
export * from "./book";
export * from "./music";
export * from "./profile";
export * from "./video";
export * from "./website";

export type MusicObject =
  | "music.song"
  | "music.album"
  | "music.playlist"
  | "music.radio_station";
export type VideoObject =
  | "video.movie"
  | "video.episode"
  | "video.tv_show"
  | "video.other";

export type OtherObject = "article " | "book " | "profile " | "website ";

export type ObjectType = MusicObject | VideoObject | OtherObject;
