import { IProfile } from "./profile";

export interface IArticle {
  publishedTime: Date | null;
  modifiedTime: Date | null;
  expirationTime: Date | null;
  authors: IProfile[];
  section: string | null;
  tags: string[];
}
