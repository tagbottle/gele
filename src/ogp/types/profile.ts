export interface IProfile {
  firstName: string | null;
  lastName: string | null;
  username: string | null;
  gender: "male" | "female" | null;
}
