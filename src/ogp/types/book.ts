import { IProfile } from "./profile";

export interface IBook {
  authors: IProfile[];
  isbn: string | null;
  releaseDate: Date | null;
  tags: string[];
}
