import { IProfile } from "./profile";

export interface IVideoMovie {
  actors: IProfile[];
  actor: string | null;
  directors: IProfile[];
  writers: IProfile[];
  duration: number | null;
  release_date: Date | null;
  tags: string[];
}

export interface IVideoEpisode {
  actors: IProfile[];
  actor: string | null;
  directors: IProfile[];
  writers: IProfile[];
  duration: number | null;
  release_date: Date | null;
  tags: string[];
  series: IVideoTvShow[];
}

export type IVideoTvShow = IVideoMovie;
export type IVideoOther = IVideoMovie;
