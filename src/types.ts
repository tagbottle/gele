import { IMetaData } from "./meta";
import { IOgpData } from "./ogp";

interface IGeleResult {
  ogp: IOgpData;
  meta: IMetaData;
}

export { IOgpData, IMetaData, IGeleResult };
