import { either } from "fp-ts";
import Failure from "./Failure";
import Success from "./Success";

export type Payload = Success | Failure;
export type Result = either.Either<Failure, Success>;

export default Result;
