import axios from "axios";
import { either } from "fp-ts";

import { Failure } from "./Failure";
import { Result } from "./Result";
import { Success } from "./Success";

export const download = async (url: string): Promise<Result> => {
  // get content from url: only GET, no post, put or others
  const response = await axios.get<string>(url);

  // return error value if GET action failed
  // currently, this accepts only status 200 as success
  if (response.status !== 200) {
    const failure = new Failure();
    return either.left(failure);
  }

  const success = new Success(response.data);
  return either.right(success);
};

export default download;
